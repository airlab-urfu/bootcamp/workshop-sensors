## Состав кита

* Экран 8x8 с контроллером MAX219CNG (более 10)
* Arduino UNO/Mega (5+ новых nano, 5+ mega)
* Sensor Shield (6+ nano, 3+ uno shield, 4+ mega shield)
* HC-SR04 2 шт. (20+ новых)
* Провод мама-мама 13 шт. (их есть у нас)

В наличии более 10 комплектов.

Готовое устройство представляет собой два дальномера (ИК и УЗ) подключенных к Arduino.
Так же к ней подключен экран 8x8, на котором бегает пиксель или спрайт, управляемый с сенсоров.


## Step 0. Показать готовое устройство
- Референс-реализация должна быть автономной
- Нужно дать потрогать руками, поиграть немного

## Step 1. Учимся работать с ИК дальномером
- Объяснить принцип работы
- Простейший скетч - принтуем analogRead в консоль
- Рассказать про то, что возвращает функция analogRead, пин AREF
- Разбираемся с SerialPlotter в ардуино IDE, смотрим график напряжения
- Обратить внимание на шумы

## Step 2. Учимся работать с фильтрами
- Делаем несколько измерений
- Откидываем самое большое и самое малое значение (считая что это выбросы)
- Считаем среднее значение
- Обратить внимание на паузу между измерениями

## Step 3. Измеряем расстояние посредством ИК дальномера
 - Показать Datasheet, график зависимости расстояния от напряжения
 - Объяснить чем обусловлены мертвые зоны (до 15см и более 80)
 - Выдать формулу для бльшого и малоко сенсора
 - Еще раз посомтреть на график - на этот раз расстояния

## Step 4. Измеряем расстояние посредством УЗ дальномера
 - Показать Datasheet





